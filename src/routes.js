import React, { useEffect } from "react";
import { useState } from "react";
import Home from "./Routes/home";
import About from "./Routes/about";
import Price from "./Routes/price";
import Error from "./Routes/error";
import Task from "./Routes/task";


import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const Routes = () => {
  return (
    <>
     <Router>
       <nav>
         <ul>
           <li>
            <Link to="/">Home</Link>
           </li>
           <li>
             <Link to="/about">About  </Link>  
             </li>
             <li>
               <Link  to="/price">Price</Link>
             </li>
         </ul>
       </nav>
     
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/price">
            <Price />
          </Route>
          <Route path="/task/:id" children={<Task/>}></Route>
          <Route path="*">
            <Error />
          </Route>
        </Switch>
      </Router>
    </>
  );
};

export default Routes;
