import logo from './logo.svg';
import './App.css';

import Header from "./hook/hook";
import Todo from "./hook/todo"
import Counter from "./hook/counter";
import Froms from "./hook/forms";
import Useref from "./hook/useRef";
import Routes from "./routes";
import Review from "./review/review";


import 'bootstrap/dist/css/bootstrap.min.css';
import "bootstrap-icons/font/bootstrap-icons.css";

function App() {

  
  return (
    <div className="App" >

     {/* <Header/>
     <Counter/> */}
     {/* <Todo /> */}
     {/* <Froms/> */}
     {/* <Useref/> */}
    {/* <Routes/> */}
      <Review />
        
    </div >
  );
}

export default App;
