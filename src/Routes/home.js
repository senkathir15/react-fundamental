import React, { useEffect } from "react";
import {useState} from "react";
import { data } from "./data";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Task from "./task"

    const Home = ()=>{

        return(

            <div>
                <h1>Home</h1>
                {
                    data.map((data)=>{

                        return (
                            <div>
                                <Link to={`task/${data.id}`} ><h1>{data.name}</h1></Link>
                            </div>
                        );
                    })
                }
            </div>
        );

    }


    export default Home;