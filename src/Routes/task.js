import React, { useEffect } from "react";
import {useState} from "react";
import { BrowserRouter as Router, Switch, Route, Link ,useParams} from "react-router-dom";
import { data } from "./data";

const Task = ()=>{
    const[name,setName]=useState("no name")

      const {id} = useParams();
    
   
      useEffect(()=>{
               const person = data.find((datas)=> parseInt(datas.id) === parseInt(id));
            setName(person.name);
          

      },[]);

    return(
        <>
         {name}  
        
        </>
    );
}

export default Task;