import React, { useEffect } from "react";
import {useState} from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card,Button } from 'react-bootstrap';
import "bootstrap-icons/font/bootstrap-icons.css";
import { ArrowRight } from 'react-bootstrap-icons';
import {data} from "./reviewdata";
import "./review.css";

const Review = ()=>{

    const [value,setValue]=useState(0);
    const check =(val)=>{

        if(val > (data.length-1)){

            return 0;
        }
        if(val < 0){

            return data.length-1;
        }

           return val;

    }

    const countinc = ()=>{
              
            setValue(
                   (value)=>{
                       let newValue = value + 1;
                        return check(newValue);
                   }
            );  
    }
   
    const countprev = ()=>{
              
            setValue(
                   (value)=>{
                       let newValue = value - 1;
                        return check(newValue);

                   }

            );  
                   
              

    }
    return(

        
        <div className="flex">
            <h1 className="head">Review</h1>
            <Card className="content" >
                    <Button   onClick={countinc} >
                    </Button>
                    <Card.Title>
                        {data[value].id}{data[value].name}
                    </Card.Title>
                     <Button   onClick={countprev} >
                     <ArrowRight color="white"  />
                    </Button>
            </Card >

        </div>
        
      
    );


}

export default Review;