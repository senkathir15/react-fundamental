import React, { useEffect } from "react";
import {useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card,Button } from 'react-bootstrap';

import "bootstrap-icons/font/bootstrap-icons.css";
import { ArrowRight } from 'react-bootstrap-icons';


 const ModifedTodo = ()=>{


    
const list = [];
const[todo,settodo]=useState(list);
const[todoo,settodoo]=useState([]);
const[value,setvalue]=useState(" NO TO DO LIST IS HERE");
const[count,setcount]=useState(0);
const[clear,setclear]=useState("");



const addTask = (text)=>{

    let dex = [...todo,{do:text}];
  
    settodo(dex);
    setvalue("Number of to do list are")
    setcount(dex.length);
    setclear("Added Sucessfully");
};


const removeTask = (val)=>{
 
    const updatelist = [...todo];
    updatelist.splice(val,1);
    
    settodo(updatelist);
    setcount(updatelist.length);
    setclear("removed Sucessfully");
    if(updatelist.length === 0){
    setvalue("NO TO DO LISTs are HERE")
    }


}
const delectAll = ()=>{
    settodo([]); 
    setclear("All Deleted sucessfully");
    setcount(0);
    setvalue("NO TO DO LIST IS HERE")
}
   let button="";
if(todo.length >1){

     button =  <Button type="button" variant="danger" onClick={delectAll}>Remove All<br></br><i class="bi bi-trash-fill"></i></Button>;
}


    return(
        <div>
            {value}
            
            <h1>{count}</h1>
            <p  >{clear}</p>
            <AddTask addTask={addTask}/>
            {button}
             {todo.map((number,index)=>{
           return( 
               
               <div className="col d-flex justify-content-center ">
                  
               
              {/* <span><h1>{number.do}</h1><Button type="button" class="fa fa-trash" onClick={()=>removeTask(index)}>remove</Button> </span> */}
              <Card style={{ width: '18rem' ,height:'7rem' ,background:"#11f29c",margin:'10px',borderRadius: '25px'}} >
                <Card.Img variant="top"  />
                <Card.Body>
                    <Card.Title></Card.Title>
                    <Card.Text>
                       {number.do}
                    </Card.Text>
                   
                    <Button  style={{ textAlign:'end',display:'inline-block',padding: 'none' }} type="button"  onClick={()=>removeTask(index)}>
                    <i class="bi bi-x-circle-fill"></i></Button>
                </Card.Body>
                
            </Card >
</div>
)
             })}
              {todoo}
              
              
             
        </div>
    );
    

 }
 const AddTask = ({addTask})=>{
    const[value,setvalue]=useState("");
    const[clear,setclear]=useState("");


const handelSubmit = (e)=>{
    e.preventDefault();
   if(value !==""){
   
    addTask(value);
     setvalue("")
     setclear("");
   }
   else {
    setclear("Please Enter the Task ");
   }
    
}

    return( <div>
                 <form onSubmit={handelSubmit}>
                     {clear}
                     <input className="form-control-md"
                     type="text"
                     value={value}
                     onChange={(e)=>{
                        setvalue(e.target.value)
                     }}/>
                  <span Style={"padding-left:10px"}></span>
                     <Button  class="btn btn-info btn-rounded btn-lg" type="submit"><ArrowRight color="white" size={25} /></Button>
                     
                 </form>
             </div>);
 }


 export default  ModifedTodo;