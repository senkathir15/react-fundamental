import React, { useEffect } from "react";
import {useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card,Button } from 'react-bootstrap';

const Forms = () =>{
    const[value,setValue]=useState({fristName:"",age:""});
   
    const [submit,setSubmit]=useState([]);
    const onchange = (e)=>{
        const name = e.target.name;
        const values = e.target.value;
        if(name && value ){

            setValue({...value,[name]:values});
            

        }

    }
    const handelSubmit = (e)=>{
        e.preventDefault();
        console.log(value);
        setValue({fristName:"",age:""});
           
    }

    return(
        <>
            <form > 
                <label>fristName:</label>
                     <input className="form-control-md"
                     name="fristName"
                     type="text"
                     value={value.fristName}
                     onChange={
                     onchange}/>
                     <label>Age</label>
                     <input className="form-control-md"
                     name="age"
                     type="text"
                     value={value.age}
                     onChange={
                     onchange}/>
                     <Button variant="success"  type="submit" onClick={handelSubmit} >add</Button>
                     
                 </form>

        </>
    );
}


export default Forms;