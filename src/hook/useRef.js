import React, { useEffect,useRef } from "react";
    import {useState} from "react";
    

    const Useref = () =>{
        const values=useRef("");
        const values1=useRef("");
        const values2=useRef("");
        const values3=useRef("");
        const values4=useRef("");

        const handelSubmit = (e)=>{
            e.preventDefault();
            console.log(values.current.value);
            console.log(values1.current.value);
            console.log(values2.current.value);
            console.log(values3.current.value);

        }

        return(
            <div>
                <form onSubmit={handelSubmit}>
                    <input type="text" ref={values} />
                    <input type="text" ref={values1} />
                    <input type="text" ref={values2} />

                    <input type="text" ref={values3} />

                    <button type="submit">submit</button>
                </form>
            </div>
        );

    }

    export default Useref;